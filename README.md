Aura.Demo
=========

"Hello World" demos for Aura; because this is a demo of the framework, it
depends on the `Aura.Framework` package.
